//
//  WUPHomeWorkCardDetailsViewModel.swift
//  WUPHomeworkTests
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import XCTest
@testable import WUPHomework


class WUPHomeWorkCardDetailsViewModel: XCTestCase {

    var cardDetailsViewModel: CardDetailsViewModel?
    
    override func setUp() {
        
        let card = Card()
        
        card.availableBalance = 5000
        card.reservations = 300
        card.currentBalance = 4400
        
        card.balanceCarriedOverFromLastStatement = 111
        card.spendingsSinceLastStatement = 222
        
        cardDetailsViewModel = CardDetailsViewModel(withCard: card)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        cardDetailsViewModel = nil
    }
    
    func testGetAvailableBalanceValue() {
        let result = cardDetailsViewModel?.getAvailableBalanceValue()
        
        XCTAssertEqual(result, 5000)
    }
    
    func testGetReservationsValue() {
        let result = cardDetailsViewModel?.getReservedValue()
        
        XCTAssertEqual(result, 300)
    }
    
    func testGetCurrentBalanceValue() {
        let result = cardDetailsViewModel?.getCurrentBalanceValue()
        
        XCTAssertEqual(result, 4400)
    }
    
    func testGetCurrentBalanceValueWhenNil() {
        cardDetailsViewModel?.card.currentBalance = nil
        let result = cardDetailsViewModel?.getCurrentBalanceValue()
        
        XCTAssertEqual(result, 0)
    }
    
    func testGetTitleForSectionAt() {
        cardDetailsViewModel?.sectionTitles = ["BALANCE","ACCOUNT","CARD"]
        let result = cardDetailsViewModel?.getTitleForSection(at: 1)
        
        XCTAssertEqual(result, "ACCOUNT")
    }
    
    func testGetTitleForSectionAtNegativeIndex() {
        cardDetailsViewModel?.sectionTitles = ["BALANCE","ACCOUNT","CARD"]
        let result = cardDetailsViewModel?.getTitleForSection(at: -1)
        
        XCTAssertEqual(result, "")
    }

    func testGetTitleForSectionAtTooLargeIndex() {
        cardDetailsViewModel?.sectionTitles = ["BALANCE","ACCOUNT","CARD"]
        let result = cardDetailsViewModel?.getTitleForSection(at: 5)
        
        XCTAssertEqual(result, "")
    }
}
