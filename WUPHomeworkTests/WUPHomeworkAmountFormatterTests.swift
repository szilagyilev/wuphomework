//
//  WUPHomeworkAmountFormatterTests.swift
//  WUPHomeworkTests
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import XCTest
@testable import WUPHomework

class WUPHomeworkAmountFormatterTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDefaultBehaviorPositiveValue() {
        let formattedAmount = AmountFormatter(withValue: 60123).format()
        XCTAssertEqual(formattedAmount, "60'123.00")
    }
    
    func testDefaultBehaviorNegativeValue() {
        let formattedAmount = AmountFormatter(withValue: -60123).format()
        XCTAssertEqual(formattedAmount, "-60'123.00")
    }
    
    func testDefaultBehaviorWithStyle() {
        let formattedAmount = AmountFormatter(withValue: 60123, style: .custom).format()
        XCTAssertEqual(formattedAmount, "60'123.00")
    }
    
    func testDefaultBehaviorWrongResult() {
        let formattedAmount = AmountFormatter(withValue: 60123).format()
        XCTAssertNotEqual(formattedAmount, "60'123")
    }

    func testDefaultBehaviorCustomGrouping() {
        let formattedAmount = AmountFormatter(withValue: 60123).groupingSeparator(string: "x").format()
        XCTAssertEqual(formattedAmount, "60x123.00")
    }
    
    func testDefaultBehaviorCustomFormat() {
        let formattedAmount = AmountFormatter(withValue: 60123).positiveResultFormat(string: "#,##0.00").format()
        XCTAssertEqual(formattedAmount, "60'123.00")
    }
    
    func testDefaultBehaviorCustomFormatCustomGrouping() {
        let formattedAmount = AmountFormatter(withValue: 60123).positiveResultFormat(string: "#,##0.00").groupingSeparator(string: "#").format()
        XCTAssertEqual(formattedAmount, "60#123.00")
    }
}
