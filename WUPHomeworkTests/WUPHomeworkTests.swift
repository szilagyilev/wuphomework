//
//  WUPHomeworkTests.swift
//  WUPHomeworkTests
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import XCTest
@testable import WUPHomework

class WUPHomeworkCustomDateParserTests: XCTestCase {

    var date: Date? = nil
    
    override func setUp() {

        var dateComponents = DateComponents()
        dateComponents.year = 2000
        dateComponents.month = 1
        dateComponents.day = 3
        dateComponents.timeZone = TimeZone(abbreviation: "GMT")
        
        let userCalendar = Calendar.current // user calendar
        date = userCalendar.date(from: dateComponents)
    }

    override func tearDown() {
        date = nil
    }

    func testCorrectInputWithDots() {
        let dateFromString =  CustomDateParser.init(withValue: "2000.01.03", style: .custom).parse()
        XCTAssertEqual(dateFromString, date)
    }
    
    func testCorrectInputWithDashes() {
        let dateFromString =  CustomDateParser.init(withValue: "2000-01-03", style: .custom).parse()
        XCTAssertEqual(dateFromString, date)
    }
    
    func testCorrectInputWithoutSeparators() {
        let dateFromString =  CustomDateParser.init(withValue: "20000103", style: .custom).parse()
        XCTAssertEqual(dateFromString, date)
    }

    func testInorrectInput() {
        let dateFromString =  CustomDateParser.init(withValue: "2000.x1.03", style: .custom).parse()
        XCTAssertNotEqual(dateFromString, date)
    }
    
    func testDifferentFormatInput() {
        let dateFromString =  CustomDateParser.init(withValue: "03.01.2001", style: .custom).parse()
        XCTAssertNotEqual(dateFromString, date)
    }
    
    func testWithCustomResultFormat() {
        let resultString =  CustomDateParser.init(withValue: "2000.01.02", style: .custom).resultFormat(string: "dd/MM/yyyy").parseToString()
        XCTAssertEqual(resultString, "02/01/2000")
    }
    
    func testWithCustomInputFormatAndSlashes() {
        let dateFromString =  CustomDateParser.init(withValue: "03/01/2000", style: .custom).inputFormat(string: "ddMMyyyy").parse()
        XCTAssertEqual(dateFromString, date)
    }
    
    func testSameTimeZone() {
        let dateFromString =  CustomDateParser.init(withValue: "2000.01.03", style: .custom).timeZoneFormat(string: "GMT").parse()
        XCTAssertEqual(dateFromString, date)
    }
    
    func testDifferentTimeZone() {
        let dateFromString =  CustomDateParser.init(withValue: "2000.01.03", style: .custom).timeZoneFormat(string: "CET").parse()
        XCTAssertNotEqual(dateFromString, date)
    }
}
