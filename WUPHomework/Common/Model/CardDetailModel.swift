//
//  CardOverViewDetailModel.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class CardDetailModel {
    
    // MARK: - Public Properties
    var title: String = "Title"
    var detail: String?
    var amount: Double?
    var currency: String?
    var isHiglighted: Bool = false
    
    var modelType: ModelType = .Text
    
    enum ModelType {
        
        case Text
        case Amount
    }
    
    // MARK: - Initializers
    init(withTitle newTitle: String, withDetail newDetail: String, isHiglighted newHiglight: Bool = false) {
        modelType = .Text
        
        title = newTitle
        detail = newDetail
        isHiglighted = newHiglight
    }
    
    init(withTitle newTitle: String, withAmount newAmount: Double, withCurrency newCurrency: String, isHiglighted newHiglight: Bool = false) {
        modelType = .Amount
        
        title = newTitle
        amount = newAmount
        currency = newCurrency
        isHiglighted = newHiglight
    }
}
