//
//  CardModel.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Card: Codable {
    
    var cardId: String?
    var issuer: String?
    var cardNumber: String?
    var expirationDate: String?
    var cardHolderName: String?
    var friendlyName: String?
    var currency: String?
    var cvv: String?
    var availableBalance: Decimal?
    var currentBalance: Decimal?
    var minPayment: Decimal?
    var dueDate: String?
    var reservations: Decimal?
    var balanceCarriedOverFromLastStatement: Decimal?
    var spendingsSinceLastStatement: Decimal?
    var yourLastRepayment: String?
    var accountDetails: AccountDetails?
    var status: String?
    var cardImage: String?
    
    enum CodingKeys: String, CodingKey {
        case cardId = "cardid"
        case issuer
        case cardNumber = "cardnumber"
        case expirationDate = "expirationdate"
        case cardHolderName = "cardholdername"
        case friendlyName = "friendlyname"
        case currency
        case cvv
        case availableBalance = "availablebalance"
        case currentBalance = "currentbalance"
        case minPayment = "minpayment"
        case dueDate = "duedate"
        case reservations
        case balanceCarriedOverFromLastStatement = "balancecarriedoverfromlaststatement"
        case spendingsSinceLastStatement = "spendingssincelaststatement"
        case yourLastRepayment = "yourlastrepayment"
        case accountDetails = "accountdetails"
        case status
        case cardImage = "cardimage"
    }
}


/* SAMPLE JSON RESPONSE
 "cardId": "1",
 "issuer": "Visa - Banca Monte Dei Paschi Di Siena S.P.A., Italy (Electron)",
 "cardNumber": "4003-1565-5402-0882",
 "expirationDate": "2021/12",
 "cardHolderName": "Mr. Shea Wiza",
 "friendlyName": "Personal Loan Account",
 "currency": "BAM",
 "cvv": "962",
 "availableBalance": 60456,
 "currentBalance": 4544,
 "minPayment": 0,
 "dueDate": "2018-09-26",
 "reservations": 3688,
 "balanceCarriedOverFromLastStatement": 856,
 "spendingsSinceLastStatement": 3688,
 "yourLastRepayment": "2018-09-26",
 "accountDetails": {
 "accountLimit": 65000,
 "accountNumber": "01-558-9"
 },
 "status": "ACTIVE",
 "cardImage": "1"
 */
