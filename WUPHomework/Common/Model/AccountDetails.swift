//
//  AccountDetails.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class AccountDetails: Codable {
    
    var accountLimit: Decimal?
    var accountNumber: String?
    
    enum CodingKeys: String, CodingKey {
        case accountLimit = "accountlimit"
        case accountNumber = "accountnumber"
    }
}
