//
//  CardDetailsTableViewSectionHeaderView.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class CardDetailsTableViewSectionHeaderView: UIView {
    
    // MARK: - Public Properties
    var title: String = "NaN" {
        didSet {
            titleTextLabel.text = title
        }
    }
    
    // MARK: - Private Properties
    private lazy var titleTextLabel: UILabel = {
        let currentBalanceTextLabel = UILabelFactory(text: "Section -1", style: .title).fontSize(of: 16).build()
        
        currentBalanceTextLabel.translatesAutoresizingMaskIntoConstraints = false
        currentBalanceTextLabel.textColor = UIColor.ThemeColor.Info.Normal
        
        return currentBalanceTextLabel
    }()
    
    // MARK: - Public Methods
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        backgroundColor = UIColor.ThemeColor.Background
        
        addSubview(titleTextLabel)
        layoutTitleTextLabel()
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting
    private func layoutTitleTextLabel() {
        
        titleTextLabel.anchor(top: self.topAnchor,
                              trailing: self.trailingAnchor,
                              bottom: self.bottomAnchor,
                              leading: self.leadingAnchor,
                              padding: UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 0))
    }
}
