//
//  CardDetailsTableViewCell.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class CardDetailsTableViewCell: UITableViewCell {
    
    // MARK: - Private Properties
    private weak var model: CardDetailModel?
    
    // MARK: Layout Elements
    private lazy var titleLabel : UILabel = {
        let titleLabel = UILabelFactory.init(text: "Title", style: .title).build()
        titleLabel.numberOfLines = 0
        
        return titleLabel
    }()
    
    private lazy var detailLabel : UILabel = {
        let detailLabel = UILabelFactory.init(text: "Value", style: .value).build()
        
        return detailLabel
    }()
    
    private lazy var higlightView : UIView = {
        let higlightView = UIView()
        higlightView.backgroundColor = UIColor.ThemeColor.Accent.Light
        
        return higlightView
    }()
    
    private lazy var amountDisplay : AmountDisplay = {
        let amountDisplay = AmountDisplay()
        
        amountDisplay.errorColor = amountDisplay.normalColor
        return amountDisplay
    }()
    
    // MARK: - Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    
    func setModel(to newModel: CardDetailModel) {
        model = newModel
        
        layoutCell()
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting Methods
    
    private func layoutCell() {
        guard let model = model else {
            return
        }
        
        if model.isHiglighted {
            addSubview(higlightView)
            layoutHighLightView()
        }
        
        addSubview(titleLabel)
        layoutCommon()
        setupCommon()
        
        switch model.modelType {
        case .Amount:
            addSubview(amountDisplay)
            layoutAmount()
            setupAmount()
        case .Text:
            addSubview(detailLabel)
            layoutText()
            setupText()
        }
    }
    
    private func layoutCommon() {
        layoutTitleLabel()
    }
    
    private func layoutHighLightView() {
        
        higlightView.anchor(top: self.topAnchor,
                            trailing: nil,
                            bottom: self.bottomAnchor,
                            leading: self.leadingAnchor)
        
        higlightView.widthAnchor.constraint(equalToConstant: 8).isActive = true
    }
    
    private func layoutTitleLabel() {
        
        titleLabel.anchor(top: self.topAnchor,
                          trailing: self.centerXAnchor,
                          bottom: self.bottomAnchor,
                          leading: self.leadingAnchor,
                          padding: UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 0))
    }
    
    private func layoutText() {
        detailLabel.anchor(top: titleLabel.topAnchor,
                           trailing: self.trailingAnchor,
                           bottom: nil,
                           leading: self.centerXAnchor,
                           padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 24))
    }
    
    private func layoutAmount() {
        amountDisplay.anchor(top: titleLabel.topAnchor,
                             trailing: self.trailingAnchor,
                             bottom: nil,
                             leading: self.centerXAnchor,
                             padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 24))
    }
    
    // MARK: Setup Methods
    private func setupCommon() {
        guard let model = model else {
            return
        }
        
        titleLabel.text = model.title
        
    }
    
    private func setupText() {
        detailLabel.textAlignment = .right
        
        guard let model = model else {
            return
        }
        
        detailLabel.text = model.detail
    }
    
    private func setupAmount() {
        
        guard let model = model,
            let amount = model.amount,
            let currency = model.currency else {
                return
        }
        
        amountDisplay.amountValue = amount
        amountDisplay.currency = currency
    }
}
