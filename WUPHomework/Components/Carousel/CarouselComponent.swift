//
//  CarouselComponent.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class CarouselComponent: UIView {
    
    // MARK: - Public Properties
    
    weak var delegate: CarouselDelegate?
    
    var items = [CarouselItemModel]() {
        didSet {
            DispatchQueue.main.async {[unowned self] in
                self.handleItemsDidSet()
            }
        }
    }
    
    var componentHeight: CGFloat {
        get {
            return itemHeight+pagingHeight
        }
    }
    
    
    var itemHeight: CGFloat = 200
    
    // MARK: - Private Properties
    
    private var cellIdentifier = "carouselItemCell"

    private var pagingHeight: CGFloat = 20
    
    private(set) var currentIndex: Int = 0 {
        didSet {
            delegate?.indexDidChange(index: currentIndex)
        }
    }
    
    // MARK: Layout Elements
    
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 6
        stackView.backgroundColor = .clear
        
        return stackView
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.register(CarouselViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = UIColor.ThemeColor.Background
        collectionView.showsHorizontalScrollIndicator = false
        
        return collectionView
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = items.count
        pc.currentPageIndicatorTintColor = UIColor.ThemeColor.Base.Normal
        pc.pageIndicatorTintColor = UIColor.ThemeColor.Base.Light
        pc.isUserInteractionEnabled = false
        
        return pc
    }()
    
    // MARK: - Public Methods
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    // MARK: - Layouting Methods
    
    override func layoutSubviews() {
        addSubview(mainStackView)
        
        layoutMainStackView()
        
        mainStackView.addArrangedSubview(collectionView)
        mainStackView.addArrangedSubview(pageControl)
        
        layoutCollectionView()
        
        layoutPageControl()
    }
    
    private func layoutMainStackView() {
        
        mainStackView.anchor(top: self.topAnchor,
                             trailing: self.trailingAnchor,
                             bottom: nil,
                             leading: self.leadingAnchor)
    }
    
    private func layoutCollectionView() {
        
        collectionView.anchor(top: mainStackView.topAnchor,
                              trailing: mainStackView.trailingAnchor,
                              bottom: nil,
                              leading: mainStackView.leadingAnchor)
        
        collectionView.heightAnchor.constraint(equalToConstant: itemHeight).isActive = true
    }
    
    private func layoutPageControl() {
        pageControl.anchor(top: nil,
                           trailing: mainStackView.trailingAnchor,
                           bottom: nil,
                           leading: mainStackView.leadingAnchor)
        pageControl.heightAnchor.constraint(equalToConstant: pagingHeight).isActive = true
    }
    
    
    // MARK: - Event handling
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let x = targetContentOffset.pointee.x
        let pageNumber = Int(x / frame.width)
        
        pageControl.currentPage = pageNumber
        currentIndex = pageNumber
    }
    
    func handleItemsDidSet() {
        collectionView.reloadData()
        pageControl.numberOfPages = items.count
        pageControl.currentPage = 0
    }
}

// MARK: - DataSource extension
extension CarouselComponent: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CarouselViewCell
        
        cell.setContent(withItem: items[indexPath.row])
        
        return cell
    }
}

// MARK: - Delegate extension
extension CarouselComponent: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: frame.width, height: itemHeight)
    }
}
