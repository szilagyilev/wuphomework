//
//  CarouselItemModel.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class CarouselItemModel {
    
    var card: Card
    
    init(withCard newCard: Card) {
        card = newCard
    }
}
