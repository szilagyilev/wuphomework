//
//  CarouselViewCell.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

//ViewController for the Carousel items
class CarouselViewCell: UICollectionViewCell {
    
    lazy var mainContentView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setContent(withItem newItem: CarouselItemModel) {
        
        layoutCell()
        setupCell(withItemModel: newItem)
    }
    
    private func layoutCell() {
        contentView.addSubview(mainContentView)
        contentView.clipsToBounds = true
        
        mainContentView.anchor(top: contentView.topAnchor,
                               trailing: contentView.trailingAnchor,
                               bottom: contentView.bottomAnchor,
                               leading: contentView.leadingAnchor)
        
        mainContentView.addSubview(imageView)
        
        imageView.anchor(top: mainContentView.topAnchor,
                         trailing: nil,
                         bottom: mainContentView.bottomAnchor,
                         leading: nil,
                         padding: UIEdgeInsets.init(top: 12, left: 0, bottom: 12, right: 0))
        
        imageView.centerXAnchor.constraint(equalTo: mainContentView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: mainContentView.centerYAnchor).isActive = true
        
        imageView.dropShadow(opacity: 0.75, radius: 6)
    }
    
    private func setupCell(withItemModel itemModel: CarouselItemModel) {
        
        if let cardImageTypeIdentifier = itemModel.card.cardImage {
            
           let cardImageIdentifier = "cardType" + cardImageTypeIdentifier
            imageView.image = UIImage(named: cardImageIdentifier)
        }
    }
}
