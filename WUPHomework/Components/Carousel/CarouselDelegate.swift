//
//  CarouselDelegate.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

protocol CarouselDelegate: class {
    
    func indexDidChange(index: Int)
}
