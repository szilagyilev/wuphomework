//
//  HorizontalBarChart.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class HorizontalBarChart: UIView {
    
    // MARK: - Public Properties
    
    var values: [Double] = []
    
    var colors: [UIColor] = []
    
    // MARK: - Private Properties
    
    lazy var mainStackView: UIStackView =  {
        
        let mainStackView = UIStackView()
        
        mainStackView.axis = .horizontal
        mainStackView.alignment = .fill
        mainStackView.spacing = 0
        mainStackView.distribution = .fill
        
        return mainStackView
    }()
    
    // MARK: - Public Methods
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        
        addSubview(mainStackView)
        
        setupLayout()
    }
    
    // MARK: - Lifecycle Methods
    
    override func layoutSubviews() {
        super.layoutSubviews()
        generateValueViews()
    }
    
    // MARK: - Private Methods
    
    func reload() {
        
        updateLayout()
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting
    
    private func generateValueViews() {
        
        let sum = values.reduce(0, +)
        let unitValue = self.frame.width / CGFloat(sum)
        
        for i in 0..<values.count {
            if values[i] > 0 {
                let newView = UIView()
                newView.backgroundColor = colors[i%colors.count]
                
                newView.translatesAutoresizingMaskIntoConstraints = false
                newView.widthAnchor.constraint(equalToConstant: unitValue * CGFloat(values[i]) ).isActive = true
                
                mainStackView.addArrangedSubview(newView)
            }
        }
    }
    
    private func setupLayout() {
        
        mainStackView.anchor(top: self.topAnchor,
                             trailing: self.trailingAnchor,
                             bottom: self.bottomAnchor,
                             leading: self.leadingAnchor)
    }
    
    private func updateLayout() {
        
        if mainStackView.arrangedSubviews.count > 0 {
            for subView in mainStackView.arrangedSubviews {
                subView.removeFromSuperview()
            }
        }
        
        if values.count > 0 && mainStackView.arrangedSubviews.count == 0 && self.frame.width > 0 {
            layoutSubviews()
        }
    }
}
