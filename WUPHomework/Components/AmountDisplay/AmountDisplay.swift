//
//  AmountDisplay.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class AmountDisplay: UIView {
    
    // MARK: - Public Properties
    var amountValue: Double = 0 {
        didSet {
            previousAmount = oldValue
            updateLayout()
        }
    }
    
    var currency: String = "NaN" {
        didSet {
            updateLayout()
        }
    }
    
    var textSize: Int = 0 {
        didSet {
            currencyLabel.font = currencyLabel.font.withSize(CGFloat(textSize))
            amountLabel.font = amountLabel.font.withSize(CGFloat(textSize))
        }
    }
    
    var textAlignment: NSTextAlignment = .right {
        didSet {
            amountLabel.textAlignment = textAlignment
        }
    }
    
    var isCurrencyVisible: Bool = false {
        didSet {
            currencyLabel.isHidden = !isCurrencyVisible
        }
    }
    
    var normalColor: UIColor = .black
    var errorColor: UIColor = UIColor.ThemeColor.Error.Normal
    
    var isAnimated: Bool = false
    var animationDurationInSeconds: Double = 0.5
    
    // MARK: - Private Properties
    
    private var previousAmount: Double = 0
    private var animationStartDate: Date = Date()
    
    // MARK: Layout Elements
    
    private lazy var currencyLabel: UILabel = {
        let currencyLabel = UILabelFactory(text: "", style: .currency).build()
        
        currencyLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return currencyLabel
    }()
    
    private lazy var amountLabel: UILabel = {
        let amountLabel = UILabelFactory(text: "", style: .value).build()
        self.normalColor = amountLabel.textColor
        self.normalColor = amountLabel.textColor
        
        amountLabel.textAlignment = .right
        amountLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return amountLabel
    }()
    
    // MARK: - Public Methods
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        addSubview(currencyLabel)
        addSubview(amountLabel)
        
        setupLayout()
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting
    private func setupLayout() {
        
        layoutCurrencyLabel()
        //setupCurrencyLabel()
        
        layoutAmountLabel()
        //setupAmountLabel()
    }
    
    private func layoutCurrencyLabel() {
        
        currencyLabel.anchor(top: nil,
                             trailing: trailingAnchor,
                             bottom: nil,
                             leading: leadingAnchor)
        
        currencyLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        currencyLabel.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    private func layoutAmountLabel() {
        
        amountLabel.anchor(top: nil,
                           trailing: trailingAnchor,
                           bottom: nil,
                           leading: leadingAnchor)
        
        amountLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        amountLabel.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    private func updateLayout() {
        amountLabel.text = AmountFormatter.init(withValue: amountValue).format()
        currencyLabel.text = currency
        
        if amountValue == 0 {
            amountLabel.textColor = errorColor
        } else {
            amountLabel.textColor =  normalColor
        }
        
        if isAnimated {
            UIView.animateKeyframes(withDuration: animationDurationInSeconds,
                                    delay: 0,
                                    options: [.calculationModePaced], animations: {
                // Add animations
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: self.animationDurationInSeconds/2, animations: {
                    self.amountLabel.alpha = 0.5
                })
                UIView.addKeyframe(withRelativeStartTime: self.animationDurationInSeconds/2, relativeDuration: self.animationDurationInSeconds/2, animations: {
                    self.amountLabel.alpha = 1.0
                })
            }, completion: nil)
        }
    }
}
