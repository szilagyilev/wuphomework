//
//  DataDisplayTableView.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class DataDisplayTableView: UITableView {
    override open var intrinsicContentSize: CGSize {
        return contentSize
    }
}
