//
//  BarComponent.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class BarComponent: UIView {
    
    // MARK: - Public Properties
    
    var primaryValue: Double = 0 {
        didSet {
            assert(primaryValue >= 0, "Invalid value")
            updateLayout()
        }
    }
    var secondaryValue: Double = 0 {
        didSet {
            assert(secondaryValue >= 0, "Invalid value")
            updateLayout()
        }
    }
    
    var primaryValueColor: UIColor = UIColor(rgb: 0x0000FF) {
        didSet {
            primaryValueView.backgroundColor = primaryValueColor
        }
    }
    
    var secondaryValueColor: UIColor = UIColor(rgb: 0x00FF00) {
        didSet {
            secondaryValueView.backgroundColor = secondaryValueColor
        }
    }
    
    var isAnimated: Bool = false
    var animationDurationInSeconds: Double = 0.5
    var animationDelayInSeconds: Double = 0
    
    var cornerRadius: CGFloat = 6
    
    // MARK: - Private Properties
    
    // MARK: Layout Elements
    
    private lazy var primaryValueView: UIView = {
        let valueView = UIView()
        valueView.backgroundColor = primaryValueColor
        valueView.translatesAutoresizingMaskIntoConstraints = false
        return valueView
    }()
    
    private lazy var secondaryValueView: UIView = {
        let valueView = UIView()
        valueView.backgroundColor = secondaryValueColor
        valueView.translatesAutoresizingMaskIntoConstraints = false
        return valueView
    }()
    
    private lazy var alertImageView: UIImageView = {
        let alertImageView = UIImageView()
        
        let originalIcon = UIImage(named: "iconAlert")
        let tintedIcon = originalIcon?.withRenderingMode(.alwaysTemplate)
        
        alertImageView.image = tintedIcon
        alertImageView.tintColor = UIColor.ThemeColor.Error.Normal
        alertImageView.backgroundColor = UIColor.ThemeColor.Background
        alertImageView.layer.masksToBounds = true
        
        return alertImageView
    }()
    
    private var secondaryValueViewLeadContraint : NSLayoutConstraint?
    
    private var secondaryValueViewLeadConstant: CGFloat = 0
    
    // MARK: - Public Methods
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(primaryValueView)
        addSubview(secondaryValueView)
        addSubview(alertImageView)
        
        setupLayout()
        updateLayout()
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting
    
    private func setupLayout() {
        
        layoutPrimaryValueView()
        
        layoutSecondaryValueView()
        setupSecondaryValueView()
        
        layoutAlertIcon()
        setupAlertIcon()
    }
    
    private func layoutPrimaryValueView() {
        
        primaryValueView.anchor(top: nil,
                                trailing: trailingAnchor,
                                bottom: nil,
                                leading: leadingAnchor)
        
        primaryValueView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1/3).isActive = true
        primaryValueView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    private func layoutSecondaryValueView() {
        
        secondaryValueView.anchor(top: nil,
                                  trailing: primaryValueView.trailingAnchor,
                                  bottom: nil,
                                  leading: nil)
        
        secondaryValueView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        secondaryValueView.centerYAnchor.constraint(equalTo: primaryValueView.centerYAnchor).isActive = true
        secondaryValueViewLeadContraint = secondaryValueView.leadingAnchor.constraint(equalTo: primaryValueView.leadingAnchor, constant: secondaryValueViewLeadConstant)
        secondaryValueViewLeadContraint?.isActive = true
    }
    
    private func layoutAlertIcon() {
        
        alertImageView.anchor(top: nil,
                              trailing: trailingAnchor,
                              bottom: nil,
                              leading: nil)
        
        alertImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        alertImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        alertImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    private func setupAlertIcon() {
        
        alertImageView.layer.cornerRadius = 10
        alertImageView.isHidden = true
    }
    
    private func setupSecondaryValueView() {
        
        secondaryValueView.layer.cornerRadius = cornerRadius
        secondaryValueView.layer.masksToBounds = true
    }
    
    private func updateLayout() {
        
        let sum = primaryValue + secondaryValue
        
        let displayUnit = sum > 0 ? (self.frame.width / CGFloat(sum)) : self.frame.width
        
        secondaryValueViewLeadConstant = displayUnit * CGFloat(primaryValue)
        secondaryValueViewLeadContraint?.constant = secondaryValueViewLeadConstant
        
        if isAnimated {
            UIView.animate(withDuration: animationDurationInSeconds,
                           delay: animationDelayInSeconds,
                           options: .curveEaseInOut,
                           animations: {
                            self.layoutIfNeeded()
            },
                           completion: {[unowned self] (finished: Bool) -> Void in
                            if self.secondaryValue == 0 {
                                self.alertImageView.isHidden = false
                            } else {
                                self.alertImageView.isHidden = true
                            }
            
            })
        }
    }
}
