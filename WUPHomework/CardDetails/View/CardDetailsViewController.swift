//
//  CardDetailsViewController.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class CardDetailsViewController: UIViewController {
    
    // MARK: - Public Properties
    var viewModel: CardDetailsViewModel?
    
    // MARK: - Private Properties
    
    private let cellId = "detailCell"
    
    private var detailsTableViewHeightContraint : NSLayoutConstraint?
    
    private var detailsTableViewHeightConstant: CGFloat = 0
    
    // MARK: Layout Elements
    
    private var containerView: UIView = {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = true
        return containerView
    }()
    
    private var headerView = UIView()
    private var detailsTableView = DataDisplayTableView()
    private var horizontalBarChart = HorizontalBarChart()
    
    private lazy var currentBalanceTextLabel: UILabel = {
        let currentBalanceTextLabel = UILabelFactory(text: "Current balance", style: .title).fontSize(of: 14).build()
        
        currentBalanceTextLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return currentBalanceTextLabel
    }()
    
    private lazy var availableBalanceTextLabel: UILabel = {
        let availableBalanceTextLabel = UILabelFactory(text: "Available", style: .title).fontSize(of: 14).build()
        
        availableBalanceTextLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return availableBalanceTextLabel
    }()
    
    private var currentBalanceAmountDisplay = AmountDisplay()
    private var availableBalanceAmountDisplay = AmountDisplay()
    
    // MARK: - Public Methods
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupCommon()
        layoutComponents()
        setupComponents()
        setupBarChart()
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting Methods
    private func layoutComponents() {
        
        view.addSubview(detailsTableView)
        layoutDetailsTableView()
        
        containerView.addSubview(headerView)
        
        containerView.bounds = CGRect.init(x: 0, y: 0, width: view.frame.width, height: 120)
        
        detailsTableView.tableHeaderView = containerView
        
        layoutHeaderContentView()
        
        headerView.addSubview(currentBalanceTextLabel)
        layoutCurrentBalanceTextLabel()
        
        headerView.addSubview(availableBalanceTextLabel)
        layoutAvailableBalanceTextLabel()
        
        headerView.addSubview(currentBalanceAmountDisplay)
        layoutCurrentBalanceAmountDisplay()
        
        headerView.addSubview(availableBalanceAmountDisplay)
        layoutAvailableBalanceAmountDisplay()
        
        headerView.addSubview(horizontalBarChart)
        layoutHorizontalBarChart()
    }
    
    private func layoutHorizontalBarChart() {
        
        horizontalBarChart.anchor(top: nil,
                                  trailing: headerView.trailingAnchor,
                                  bottom: headerView.bottomAnchor,
                                  leading: headerView.leadingAnchor,
                                  padding: UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24))
        
        horizontalBarChart.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func layoutAvailableBalanceAmountDisplay() {
        
        availableBalanceAmountDisplay.anchor(top: availableBalanceTextLabel.bottomAnchor,
                                             trailing: availableBalanceTextLabel.trailingAnchor,
                                             bottom: nil,
                                             leading: headerView.centerXAnchor)
        
        availableBalanceAmountDisplay.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
    
    private func layoutCurrentBalanceAmountDisplay() {
        
        currentBalanceAmountDisplay.anchor(top: currentBalanceTextLabel.bottomAnchor,
                                           trailing: headerView.centerXAnchor,
                                           bottom: nil,
                                           leading: currentBalanceTextLabel.leadingAnchor)
        
        currentBalanceAmountDisplay.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
    
    private func layoutAvailableBalanceTextLabel() {
        availableBalanceTextLabel.anchor(top: headerView.topAnchor,
                                         trailing: headerView.trailingAnchor,
                                         bottom: nil,
                                         leading: nil,
                                         padding: UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 24))
    }
    
    private func layoutCurrentBalanceTextLabel() {
        currentBalanceTextLabel.anchor(top: headerView.topAnchor,
                                       trailing: nil,
                                       bottom: nil,
                                       leading: headerView.leadingAnchor,
                                       padding: UIEdgeInsets(top: 8, left: 24, bottom: 0, right: 0))
    }
    
    private func layoutHeaderContentView() {
        
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    }
    
    private func layoutDetailsTableView() {
        
        detailsTableView.anchor(top: view.topAnchor,
                                trailing: view.trailingAnchor,
                                bottom: view.bottomAnchor,
                                leading: view.leadingAnchor)
    }
    
    private func setupCommon() {
        
        view.backgroundColor = UIColor.ThemeColor.Background
    }
    
    // MARK: Setup Methods

    private func setupComponents() {
        
        setupDetailsTableView()
        setupCurrentBalanceAmountDisplay()
        setupAvailableBalanceAmountDisplay()
        setupBarChart()
    }
    
    private func setupBarChart() {
        
        guard let viewModel = viewModel else {
            return
        }
        
        horizontalBarChart.colors = [UIColor.ThemeColor.Accent.Normal, UIColor.ThemeColor.Accent.Light, UIColor.ThemeColor.Base.Normal]
        
        horizontalBarChart.values = [viewModel.getCurrentBalanceValue(),viewModel.getReservedValue(),viewModel.getAvailableBalanceValue()]
        horizontalBarChart.reload()
    }
    
    private func setupAvailableBalanceAmountDisplay() {
        
        availableBalanceAmountDisplay.textSize = 20
        availableBalanceAmountDisplay.isCurrencyVisible = false
        availableBalanceAmountDisplay.isAnimated = true
        availableBalanceAmountDisplay.normalColor = UIColor.ThemeColor.Base.Dark
        
        guard let viewModel = viewModel else {
            return
        }
        
        availableBalanceAmountDisplay.amountValue = viewModel.getAvailableBalanceValue()
    }
    
    private func setupCurrentBalanceAmountDisplay() {
        
        currentBalanceAmountDisplay.textSize = 20
        currentBalanceAmountDisplay.isCurrencyVisible = false
        currentBalanceAmountDisplay.isAnimated = true
        currentBalanceAmountDisplay.textAlignment = .left
        currentBalanceAmountDisplay.normalColor = UIColor.ThemeColor.Accent.Dark
        
        guard let viewModel = viewModel else {
            return
        }
        
        currentBalanceAmountDisplay.amountValue = viewModel.getCurrentBalanceValue()
    }
    
    private func setupDetailsTableView() {
        detailsTableView.tableFooterView = UIView()
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        detailsTableView.estimatedRowHeight = 44
        detailsTableView.rowHeight = UITableView.automaticDimension
        
        detailsTableView.register(CardDetailsTableViewCell.self, forCellReuseIdentifier: cellId)
    }
}

// MARK: - Extensions

extension CardDetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        guard let viewModel = viewModel else {
            return 0
        }
        
        return viewModel.cardDetailItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let viewModel = viewModel else {
            return 0
        }
        
        return viewModel.cardDetailItems[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let newCell = CardDetailsTableViewCell()
        
        guard let viewModel = viewModel else {
            return UITableViewCell()
        }
        
        newCell.setModel(to: viewModel.cardDetailItems[indexPath.section][indexPath.row])
        
        newCell.selectionStyle = .none
        return newCell
    }
}

extension CardDetailsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let sectionHeaderView = CardDetailsTableViewSectionHeaderView()
        
        if let viewModel = viewModel {
            sectionHeaderView.title = viewModel.getTitleForSection(at: section)
        }
        
        return sectionHeaderView
    }
}

