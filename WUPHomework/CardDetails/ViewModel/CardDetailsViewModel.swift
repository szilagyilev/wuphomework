//
//  CardDetailsViewModel.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class CardDetailsViewModel {
    
    // MARK: - Public Properties
    var card: Card
    
    var cardDetailItems: [[CardDetailModel]] = [[]]
    
    var sectionTitles: [String] = []
    
    // MARK: - Initializers
    init(withCard newCard: Card) {
        card = newCard
        createCardDetailItemsFor(card: card)
    }
    
    // MARK: - Public Methods
    func getAvailableBalanceValue() -> Double {
        
        var primaryValue: Double = 0
        
        if let newValue = card.availableBalance {
                primaryValue = (newValue as NSNumber).doubleValue
        }
        
        return primaryValue
    }
    
    func getReservedValue() -> Double {
        
        var reservedValue: Double = 0
        
        if let newValue = card.reservations {
            reservedValue = (newValue as NSNumber).doubleValue
        }
        
        return reservedValue
    }
    
    func getCurrentBalanceValue() -> Double {
        
        var currentBalance: Double = 0
        
        if let newValue = card.currentBalance {
                currentBalance = (newValue as NSNumber).doubleValue
        }
        
        return currentBalance
    }
    
    func getTitleForSection(at index: Int) -> String {
        
        var title = ""
        
        if 0 ..< sectionTitles.count ~= index {
            title = sectionTitles[index]
        }
        
        return title
    }
    
    func createCardDetailItemsFor(card: Card) {
        cardDetailItems.removeAll()
        
        var currencyValue: String = "-"
        
        if let newValue = card.currency {
                currencyValue = newValue
        }
        
        // RESERVATIONS
        let reservationItems = createReservationsItems(withCurrency: currencyValue)
        
        // BALANCE OVERVIEW
        let balanceOverviewItems = createBalanceOverviewItems(withCurrency: currencyValue)
        
        // ACCOUNT DETAILS
        let accountDetailsItems = createAccountDetailItems(withCurrency: currencyValue)
        
        // MAIN CARD
        let mainCardItems = createMainCardDetailItems(withCurrency: currencyValue)
        
        cardDetailItems.append(reservationItems)
        cardDetailItems.append(balanceOverviewItems)
        cardDetailItems.append(accountDetailsItems)
        cardDetailItems.append(mainCardItems)
    }

    // MARK: - Private Methods
    
    private func createReservationsItems(withCurrency currencyValue: String) -> [CardDetailModel] {
        
        var newNumber: Double = 0
        
        var reservationItems: [CardDetailModel] = []
        sectionTitles.append("")
        
        if let newValue = card.reservations {
            newNumber = (newValue as NSNumber).doubleValue
            reservationItems.append(CardDetailModel.init(withTitle: "Reservations/Pending (sum)", withAmount: newNumber, withCurrency: currencyValue, isHiglighted: true))
        }
        
        return reservationItems
    }
    
    private func createBalanceOverviewItems(withCurrency currencyValue: String) -> [CardDetailModel] {
        
        var newNumber: Double = 0
        var newString: String? = "-"
        
        var balanceOverviewItems: [CardDetailModel] = []
        sectionTitles.append("BALANCE OVERVIEW")
        
        if let newValue = card.balanceCarriedOverFromLastStatement {
            newNumber = (newValue as NSNumber).doubleValue
            balanceOverviewItems.append(CardDetailModel.init(withTitle: "Balance carried over from last statement", withAmount: newNumber, withCurrency: currencyValue))
        }
        
        if let newValue = card.spendingsSinceLastStatement {
            newNumber = (newValue as NSNumber).doubleValue
            balanceOverviewItems.append(CardDetailModel.init(withTitle: "Total spendings since last statement", withAmount: newNumber, withCurrency: currencyValue))
        }
        
        if let newValue = card.yourLastRepayment {
            newString = CustomDateParser.init(withValue: newValue, style: .custom).parseToString()
        }
        
        if let newString = newString {
            balanceOverviewItems.append(CardDetailModel.init(withTitle: "Your latest re-payment", withDetail: newString))
        }
        
        return balanceOverviewItems
    }
    
    private func createAccountDetailItems(withCurrency currencyValue: String) -> [CardDetailModel] {
        
        var newNumber: Double = 0
        var newString: String? = "-"
        
        var accountDetailsItems: [CardDetailModel] = []
        sectionTitles.append("ACCOUNT DETAILS")
        
        if let newValue = card.accountDetails?.accountLimit {
            newNumber = (newValue as NSNumber).doubleValue
            accountDetailsItems.append(CardDetailModel.init(withTitle: "Card account limit", withAmount: newNumber, withCurrency: currencyValue))
        }
        
        if let newValue = card.accountDetails?.accountNumber {
            newString = newValue
        }
        
        if let newString = newString {
            accountDetailsItems.append(CardDetailModel.init(withTitle: "Card account number", withDetail: newString))
        }
        
        return accountDetailsItems
    }
    
    private func createMainCardDetailItems(withCurrency currencyValue: String) -> [CardDetailModel] {
        
        var newString: String? = "-"
        
        var mainCardItems: [CardDetailModel] = []
        sectionTitles.append("MAIN CARD")
        
        if let newValue = card.cardNumber {
            newString = "****-****-****-" + newValue.suffix(4)
        }
        
        if let newString = newString {
            mainCardItems.append(CardDetailModel.init(withTitle: "Card number", withDetail: newString))
        }
        
        if let newValue = card.cardHolderName {
            newString = newValue
        }
        
        if let newString = newString {
            mainCardItems.append(CardDetailModel.init(withTitle: "Card holder name", withDetail: newString))
        }
        
        return mainCardItems
    }
}
