//
//  ServiceError.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

enum ServiceError: Error {
    case error(String)
}
