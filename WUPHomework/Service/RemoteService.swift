//
//  RepositoryService.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

protocol RemoteService {

    func fetchObjects<T: Decodable>(urlString: String, completion: @escaping(Result<T,Error>, String) -> ())
}
