//
//  RepositoryServiceImpl.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class RemoteServiceImpl: RemoteService {
    
    func fetchObjects<T: Decodable>(urlString: String, completion: @escaping(Result<T,Error>, String) -> ()) {
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            do {
                guard let data = data else {
                    return
                }
                
                //Check if we got a response and the status code is 2xx (success)
                do {
                guard let httpResponse = response as? HTTPURLResponse , 200 ... 299 ~= httpResponse.statusCode else {
                    print("Something went wrong! Info: \((response as! HTTPURLResponse).statusCode)")
                    
                    throw ServiceError.error("\((response as! HTTPURLResponse).statusCode)")
                }
                } catch let networkError {
                    completion(.failure(networkError), "\(networkError)")
                }
                
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .custom { keys in
                        let lastKey = keys.last!
                        if lastKey.intValue != nil {
                            return lastKey
                        }
                        let modifiedKey = lastKey.stringValue.lowercased()
                        return CustomCodingKey(stringValue: modifiedKey)!
                    }
                    
                    let object = try decoder.decode(T.self, from: data)
                    completion(.success(object), "Successfully parsed.")
                } catch let jsonError {
                    print("Failed to read feed: \(jsonError)")
                    completion(.failure(jsonError), "\(jsonError)")
                }
            }
            
            
            }.resume()
    }
}
