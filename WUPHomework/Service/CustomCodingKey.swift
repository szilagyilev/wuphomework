//
//  CustomCodingKey.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

struct CustomCodingKey: CodingKey {
    
    // MARK: - Public Properties
    var stringValue: String
    
    var intValue: Int? {
        return nil
    }
    
    // MARK: - Initializers
    init?(stringValue: String) {
        self.stringValue = stringValue
    }
    
    init?(intValue: Int) {
        return nil
    }
    
}
