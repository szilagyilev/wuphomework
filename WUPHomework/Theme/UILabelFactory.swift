//
//  UILabelFactory.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

final class UILabelFactory {
    
    // MARK: - Public Properties
    enum Style {
        
        case title
        case currency
        case value
    }
    
    // MARK: - Private Properties
    private let label: UILabel
    
    // MARK: - Initializers
    init(text: String, style: Style = .title) {
        
        label = UILabel()
        label.text = text
        label.translatesAutoresizingMaskIntoConstraints = false
        
        switch style {
            case .title: titleStyle()
            case .currency: currencyStyle()
            case .value: valueStyle()
        }
    }
    
    // MARK: - Public Methods
    func fontSize(of size: CGFloat) -> Self {
        label.font = label.font.withSize(size)
        
        return self
    }
    
    func textColor(with color: UIColor) -> Self {
        label.textColor = color
        
        return self
    }
    
    func numberOf(lines: Int) -> Self {
        label.numberOfLines = lines
        
        return self
    }
    
    func build() -> UILabel {
        
        return label
    }
    
    // MARK: - Private Methods
    private func titleStyle() {
        label.font = label.font.withSize(16)
        label.textColor = UIColor.ThemeColor.Base.Dark
        
    }
    
    private func currencyStyle() {
        label.font = label.font.withSize(16)
        label.textColor = UIColor.ThemeColor.Info.Normal
        
    }
    
    private func valueStyle() {
        label.font = label.font.withSize(16)
        label.textColor = UIColor.ThemeColor.Base.VeryDark
        
    }
}
