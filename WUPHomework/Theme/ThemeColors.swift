//
//  ThemeColors.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

extension UIColor {
    
    struct ThemeColor {
        struct Base {
            static let Light = UIColor(rgb: 0x539AC6).withAlphaComponent(0.3)
            static let Normal = UIColor(rgb: 0x539AC6)
            static let Dark = UIColor(rgb: 0x004E7B)
            static let VeryDark = UIColor(rgb: 0x05293E)
        }
        
        struct Accent {
            static let Light = UIColor(rgb: 0xF77F00).withAlphaComponent(0.2)
            static let Normal = UIColor(rgb: 0xF77F00).withAlphaComponent(0.6)
            static let Dark = UIColor(rgb: 0xF77F00)
        }
        
        struct Error {
            static let Normal = UIColor(rgb: 0xCE3760)
        }
        
        struct Info {
            static let Normal = UIColor(rgb: 0x999999)
        }
        
        static let Background = UIColor.white
    }
}
