//
//  DateFormatter.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

final class CustomDateParser {
    
    // MARK: - Public Properties
    let formatter = DateFormatter()
    
    enum Style {
        
        case custom
    }
    
    // MARK: - Private Properties
    private var value: String = ""
    
    private var resultFormatString = "dd.MM.yyyy"
    private var inputFormatString = "yyyyMMdd"
    private var timeZoneString = "GMT"
    
    // MARK: - Initializers
    init(withValue newValue: String, style: Style = .custom) {
        
        value = newValue
        switch style {
        case .custom: customStyle()
        }
    }
    
    // MARK: - Public Methods
    func resultFormat(string: String) -> Self {
        resultFormatString = string
        
        return self
    }
    
    func inputFormat(string: String) -> Self {
        inputFormatString = string
        formatter.dateFormat = inputFormatString
        
        return self
    }
    
    func timeZoneFormat(string: String) -> Self {
        timeZoneString = string
        formatter.timeZone = TimeZone(abbreviation: timeZoneString)
        
        return self
    }
    
    func parse() -> Date? {
        return formatter.date(from: value.filter("01234567890".contains))
    }
    
    func parseToString() -> String? {
        
        guard let result = formatter.date(from: value.filter("01234567890".contains)) else {
            return nil
        }
        
        formatter.dateFormat = resultFormatString
        return formatter.string(from: result)
    }
    
    // MARK: - Private Methods
    private func customStyle() {
        
        formatter.dateFormat = inputFormatString
        formatter.timeZone = TimeZone(abbreviation: timeZoneString)
    }
}
