//
//  UIButtonFactory.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

final class UIButtonFactory {
    
    // MARK: - Public Properties
    enum Style {
        
        case normal
    }
    
    // MARK: - Private Properties
    private let button: UIButton
    
    // MARK: - Initializers
    init(text: String, style: Style = .normal) {
        
        button = UIButton()
        button.setTitle(text, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        switch style {
            case .normal: normalStyle()
        }
    }
    
    // MARK: - Public Methods
    func build() -> UIButton {
        
        return button
    }
    
    // MARK: - Private Methods
    private func normalStyle() {
        
        button.titleLabel!.font = button.titleLabel!.font.withSize(14)
        button.setTitleColor(UIColor.ThemeColor.Base.Normal, for: .normal)
        button.backgroundColor = UIColor.ThemeColor.Background
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.ThemeColor.Base.Normal.cgColor
        
    }
}
