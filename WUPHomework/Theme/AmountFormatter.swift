//
//  AmountFormatter.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

final class AmountFormatter {
    
    // MARK: - Private Properties
    private let formatter = NumberFormatter()
    
    private var value: Double = 0
    
    private var positiveFormatString = "#,##0.00"
    private var groupingSeparatorString = "\'"
    
    enum Style {
        
        case custom
    }

    // MARK: - Initializers
    init(withValue newValue: Double, style: Style = .custom) {
        
        value = newValue
        switch style {
            case .custom: customStyle()
        }
    }
    
    // MARK: - Public Methods
    func positiveResultFormat(string: String) -> Self {
        positiveFormatString = string
        formatter.positiveFormat = positiveFormatString
        
        return self
    }
    
    func groupingSeparator(string: String) -> Self {
        groupingSeparatorString = string
        formatter.groupingSeparator = groupingSeparatorString
        
        return self
    }
    
    func format() -> String {
        return formatter.string(from: NSNumber.init(value: value)) ?? ""
    }
    
    // MARK: - Private Methods
    private func customStyle() {
        
        formatter.positiveFormat = positiveFormatString
        formatter.groupingSeparator = groupingSeparatorString
    }
}
