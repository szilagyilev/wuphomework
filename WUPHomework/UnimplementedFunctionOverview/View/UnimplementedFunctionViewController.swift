//
//  UnimplementedFunctionViewController.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class UnimplementedFunctionViewController: UIViewController {
    
    // MARK: - Private Properties
    // MARK: Layout Elements
    private lazy var unAvailableTextLabel: UILabel = {
        let unAvailableTextLabel = UILabelFactory(text: "This function is currently not available.\nPlease check back at a later time!", style: .title).fontSize(of: 14).build()
        
        unAvailableTextLabel.textColor = UIColor.ThemeColor.Error.Normal
        
        unAvailableTextLabel.translatesAutoresizingMaskIntoConstraints = false
        unAvailableTextLabel.textAlignment = .center
        unAvailableTextLabel.numberOfLines = 0
        
        return unAvailableTextLabel
    }()
    
    // MARK: - Public Methods
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupCommon()
        layoutComponents()
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting Methods
    private func layoutComponents() {
        
        view.addSubview(unAvailableTextLabel)
        layoutUnavailableTextLabel()
    }
    
    private func layoutUnavailableTextLabel() {
        
        unAvailableTextLabel.anchor(top: nil,
                                    trailing: view.trailingAnchor,
                                    bottom: nil,
                                    leading: view.leadingAnchor,
                                    padding: UIEdgeInsets(top: 0, left: 48, bottom: 0, right: 48))
        
        unAvailableTextLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        unAvailableTextLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    private func setupCommon() {
        view.backgroundColor = UIColor.ThemeColor.Background
    }
}
