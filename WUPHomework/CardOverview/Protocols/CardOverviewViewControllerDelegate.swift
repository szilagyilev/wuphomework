//
//  CardOverviewViewControllerDelegate.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

protocol CardOverviewViewControllerDelegate: class {
    func cardOverviewDidSelectCard(_ selectedCard: Card)
}
