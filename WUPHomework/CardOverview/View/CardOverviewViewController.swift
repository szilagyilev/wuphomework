//
//  CardOverviewView.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class CardOverviewViewController: UIViewController {

    // MARK: - Public Properties
    var viewModel: CardOverviewViewModel?
    var delegate: CardOverviewViewControllerDelegate? //TODO: This should be weak.
    
    // MARK: - Private Properties
    
    let cellId = "detailCell"
    
    private var detailsTableViewHeightContraint : NSLayoutConstraint?
    
    private var detailsTableViewHeightConstant: CGFloat = 0
    
    // MARK: Layout Elements
    
    private let activityView = UIActivityIndicatorView(style: .gray)
    
    private var barComponent = BarComponent()
    private var carouselComponent = CarouselComponent()
    private var currentBalanceAmountDisplay = AmountDisplay()
    private var detailsTableView = DataDisplayTableView()
    
    private var mainScrollView = UIScrollView()
    private var contentView = UIView()
    
    private lazy var retryButton: UIButton = {
        let retryButton = UIButtonFactory(text: "RETRY").build()
        
        retryButton.isHidden = true
        
        retryButton.addTarget(self, action: #selector(retryButtonPressed), for: .touchUpInside)
        return retryButton
    }()
    
    private lazy var detailsButton: UIButton = {
        let detailsButton = UIButtonFactory(text: "DETAILS").build()
        
        detailsButton.isEnabled = false
        
        detailsButton.addTarget(self, action: #selector(detailsButtonPressed), for: .touchUpInside)
        return detailsButton
    }()
    
    private lazy var availableTextLabel: UILabel = {
        let availableTextLabel = UILabelFactory(text: "Available", style: .title).fontSize(of: 14).build()
        
        availableTextLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return availableTextLabel
    }()
    
    // MARK: - Public Methods
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupCommon()
        
        layoutComponents()
        setupComponents()
        
        setupViewModel()
        
        guard let viewModel = viewModel else {
            return
        }
        
        viewModel.fetchCards()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = UIColor.ThemeColor.Base.Dark
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.ThemeColor.Base.Dark]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.largeTitleTextAttributes = textAttributes
    }
    
    // MARK: - Private Methods
    
    // MARK: Layouting Methods
    private func layoutComponents() {
        
        view.addSubview(mainScrollView)
        layoutMainScrollView()
        
        mainScrollView.addSubview(contentView)
        layoutContentView()
        
        contentView.addSubview(carouselComponent)
        layoutCarouselComponent()

        contentView.addSubview(activityView)
        layoutActivityView()

        contentView.addSubview(currentBalanceAmountDisplay)
        layoutCurrentBalanceAmountDisplay()

        contentView.addSubview(availableTextLabel)
        layoutAvailableTextLabel()

        contentView.addSubview(barComponent)
        layoutBarComponent()

        contentView.addSubview(detailsTableView)
        layoutDetailsTableView()

        contentView.addSubview(detailsButton)
        layoutDetailsButton()
        
        contentView.addSubview(retryButton)
        layoutRetryButton()
        
    }
    
    private func layoutMainScrollView() {
        
        mainScrollView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                              trailing: view.safeAreaLayoutGuide.trailingAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              leading: view.safeAreaLayoutGuide.leadingAnchor)
    }
    
    private func layoutContentView() {
        
        contentView.anchor(top: mainScrollView.topAnchor,
                           trailing: mainScrollView.trailingAnchor,
                           bottom: mainScrollView.bottomAnchor,
                           leading: mainScrollView.leadingAnchor)
        
        contentView.widthAnchor.constraint(equalTo: mainScrollView.widthAnchor).isActive = true
    }
    
    private func layoutActivityView() {
        
        activityView.center = CGPoint.init(x: view.center.x, y: view.center.y * (2/5))
    }
    
    private func layoutCarouselComponent() {
        
        carouselComponent.anchor(top: contentView.topAnchor,
                                 trailing: contentView.trailingAnchor,
                                 bottom: nil,
                                 leading: contentView.leadingAnchor,
                                 padding: UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0))
        
        carouselComponent.heightAnchor.constraint(equalToConstant: carouselComponent.componentHeight).isActive = true
    }
    
    private func layoutCurrentBalanceAmountDisplay() {
        
        currentBalanceAmountDisplay.anchor(top: carouselComponent.bottomAnchor,
                                           trailing: carouselComponent.trailingAnchor,
                                           bottom: nil,
                                           leading: carouselComponent.centerXAnchor,
                                           padding: UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 24))
        
        currentBalanceAmountDisplay.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
    
    private func layoutAvailableTextLabel() {
        
        availableTextLabel.anchor(top: nil,
                                  trailing: carouselComponent.centerXAnchor,
                                  bottom: currentBalanceAmountDisplay.bottomAnchor,
                                  leading: carouselComponent.leadingAnchor,
                                  padding: UIEdgeInsets(top: 0, left: 24, bottom: 8, right: 0))
        
    }
    
    private func layoutBarComponent() {
        
        barComponent.anchor(top: currentBalanceAmountDisplay.bottomAnchor,
                            trailing: carouselComponent.trailingAnchor,
                            bottom: nil,
                            leading: carouselComponent.leadingAnchor,
                            padding: UIEdgeInsets(top: 6, left: 24, bottom: 0, right: 24))
        
        barComponent.heightAnchor.constraint(equalToConstant: 12).isActive = true
    }
    
    func layoutDetailsTableView() {
        
        detailsTableView.anchor(top: barComponent.bottomAnchor,
                                trailing: contentView.trailingAnchor,
                                bottom: nil,
                                leading: contentView.leadingAnchor,
                                padding: UIEdgeInsets(top: 32, left: 0, bottom: 0, right: 0))
        
        detailsTableViewHeightContraint = detailsTableView.heightAnchor.constraint(equalToConstant: detailsTableViewHeightConstant)
        detailsTableViewHeightContraint?.isActive = true
    }
    
    func layoutRetryButton() {
        
        retryButton.widthAnchor.constraint(equalToConstant: 160).isActive = true
        retryButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: activityView.centerXAnchor).isActive = true
        retryButton.centerYAnchor.constraint(equalTo: activityView.centerYAnchor).isActive = true
    }
    
    func layoutDetailsButton() {
        
        detailsButton.anchor(top: detailsTableView.bottomAnchor,
                             trailing: nil,
                             bottom: nil,
                             leading: nil,
                             padding: UIEdgeInsets(top: 32, left: 0, bottom: 0, right: 0))
        
        detailsButton.widthAnchor.constraint(equalToConstant: 160).isActive = true
        detailsButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        detailsButton.centerXAnchor.constraint(equalTo: carouselComponent.centerXAnchor).isActive = true
        
        detailsButton.bottomAnchor.constraint(equalTo: mainScrollView.bottomAnchor, constant: 0).isActive = true
    }
    
    // MARK: Setup Methods
    private func setupComponents() {
        
        setupCarouselComponent()
        setupCurrentBalanceAmountDisplay()
        setupBarComponent()
        setupDetailsTableView()
    }
    
    func setupDetailsTableView() {
        detailsTableView.tableFooterView = UIView()
        detailsTableView.dataSource = self
        detailsTableView.isScrollEnabled = false
        
        detailsTableView.register(CardDetailsTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    private func setupCommon() {
        view.backgroundColor = UIColor.ThemeColor.Background
    }
    
    private func setupCurrentBalanceAmountDisplay() {
        
        currentBalanceAmountDisplay.textSize = 20
        currentBalanceAmountDisplay.isCurrencyVisible = false
        currentBalanceAmountDisplay.isAnimated = true
    }
    
    private func setupBarComponent() {
        
        barComponent.primaryValueColor = UIColor.ThemeColor.Accent.Normal
        barComponent.secondaryValueColor = UIColor.ThemeColor.Base.Normal
        barComponent.isAnimated = true
    }
    
    private func setupCarouselComponent() {
        
        carouselComponent.backgroundColor = .clear
        
        carouselComponent.delegate = self
    }
    
    private func setupViewModel() {
        
        guard let viewModel = viewModel else {
            return
        }
        
        viewModel.itemsRetrievedHandler = { [unowned self] in
            self.carouselComponent.items = viewModel.carouselItems
            
            DispatchQueue.main.async {[unowned self] in
                self.activityView.stopAnimating()
                self.detailsButton.isEnabled = true
                self.indexDidChange(index: 0)
                self.retryButton.isHidden = true
                self.detailsButton.isHidden = false
            }
        }
        
        viewModel.itemsNotRetrievedHandler = { errorMessage in
            DispatchQueue.main.async {[unowned self] in
                self.showAlert(title: "Error", message: errorMessage)
                self.activityView.stopAnimating()
                self.retryButton.isHidden = false
                self.detailsButton.isHidden = true
            }
        }
        
        viewModel.fetchCards()
        activityView.startAnimating()
    }
    
    private func showAlert(title: String, message: String) {
        let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // add close button
        let cancelAction: UIAlertAction = UIAlertAction(title: "Close", style: .cancel) { _ in }
        actionSheetController.addAction(cancelAction)
        
        // show on self
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    //MARK: - Event handlers
    @objc private func detailsButtonPressed() {

        guard let viewModel = viewModel, let selectedCard = viewModel.getCard(at: carouselComponent.currentIndex) else {
            return
        }
        
        delegate?.cardOverviewDidSelectCard(selectedCard)
    }
    
    @objc private func retryButtonPressed() {
        
        guard let viewModel = viewModel else {
            return
        }
        
        self.retryButton.isHidden = true
        viewModel.fetchCards()
        activityView.startAnimating()
    }
}

// MARK: - Extensions
extension CardOverviewViewController: CarouselDelegate {
    
    func indexDidChange(index: Int) {
        
        guard let viewModel = viewModel else {
            return
        }
        
        let availableBalanceValue = viewModel.getAvailableBalanceValueForCard(at: index)
        let currentBalanceValue = viewModel.getCurrentBalanceValueForCard(at: index)
        
        barComponent.primaryValue = currentBalanceValue
        barComponent.secondaryValue = availableBalanceValue
        
        currentBalanceAmountDisplay.amountValue = availableBalanceValue
        currentBalanceAmountDisplay.currency = viewModel.getCurrencyForCard(at: index)
        
        viewModel.createCardDetailItemsForCard(at: index)
        
        DispatchQueue.main.async {[unowned self] in
            self.detailsTableView.reloadData()
            
            self.detailsTableViewHeightConstant = self.detailsTableView.contentSize.height
            self.detailsTableViewHeightContraint?.constant = self.detailsTableViewHeightConstant
        }
    }
}

extension CardOverviewViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let viewModel = viewModel else {
            return 0
        }
        
        return viewModel.cardDetailItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let newCell = CardDetailsTableViewCell()

        guard let viewModel = viewModel else {
            return UITableViewCell()
        }

        newCell.setModel(to: viewModel.cardDetailItems[indexPath.row])

        newCell.selectionStyle = .none
        return newCell
    }
}
