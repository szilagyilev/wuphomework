//
//  CardOverviewViewModel.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class CardOverviewViewModel {
    
    // MARK: - Public Properties
    let remoteService: RemoteService
    
    var carouselItems: [CarouselItemModel] = []
    
    var itemsRetrievedHandler: (()->Void)?
    
    var itemsNotRetrievedHandler: ((String)->Void)?
    
    var cardDetailItems: [CardDetailModel] = []
    
    var remoteURL: String = "https://demo7270439.mockable.io/wupdigital/api/v1/card"
    
    // MARK: - Initializers
    init(withRemoteService newRemoteService: RemoteService ) {
        
        remoteService = newRemoteService
    }
    
    // MARK: - Public Methods
    func fetchCards() {
        
        remoteService.fetchObjects(urlString: remoteURL) { (result: Result<[Card], Error>, message: String)  in
            
            switch result {
            case .success(let cardsResponse):
                self.carouselItems.removeAll()
    
                for newCard in cardsResponse {
                    self.carouselItems.append(CarouselItemModel.init(withCard: newCard))
                }
    
                if self.itemsRetrievedHandler != nil {
                    self.itemsRetrievedHandler!()
                }
                
            case .failure(_):
                if self.itemsNotRetrievedHandler != nil {
                    self.itemsNotRetrievedHandler!(message)
                }
            }
        }
    }
    
    func getAvailableBalanceValueForCard(at index: Int) -> Double {
        
        var primaryValue: Double = 0
        
        if carouselItems.count > 0 && index < carouselItems.count {
            if let newValue = carouselItems[index].card.availableBalance {
                primaryValue = (newValue as NSNumber).doubleValue
            }
        }
        
        return primaryValue
    }
    
    func getCurrentBalanceValueForCard(at index: Int) -> Double {
        
        var secondaryValue: Double = 0
        
        if carouselItems.count > 0 && index < carouselItems.count {
            if let newValue = carouselItems[index].card.currentBalance {
                secondaryValue = (newValue as NSNumber).doubleValue
            }
        }
        
        return secondaryValue
    }
    
    func getCurrencyForCard(at index: Int) -> String {
        
        var currency = "-"
        
        if carouselItems.count > 0 && index < carouselItems.count {
            if let newCurrency = carouselItems[index].card.currency {
                currency = newCurrency
            }
        }
        
        return currency
    }
    
    func createCardDetailItemsForCard(at index: Int) {
        
        cardDetailItems.removeAll()
        
        let currentBalanceValue: Double = getCurrentBalanceValueForCard(at: index)
        
        var minPaymentValue: Double = 0
        
        if carouselItems.count > 0 && index < carouselItems.count {
            if let newValue = carouselItems[index].card.minPayment {
                minPaymentValue = (newValue as NSNumber).doubleValue
            }
        }
        
        var dueDateString: String?
        
        if carouselItems.count > 0 && index < carouselItems.count {
            if let newValue = carouselItems[index].card.dueDate {
                dueDateString = CustomDateParser.init(withValue: newValue, style: .custom).parseToString()
            }
        }
        
        var currencyValue: String = "-"
        
        if carouselItems.count > 0 && index < carouselItems.count {
            if let newValue = carouselItems[index].card.currency {
                currencyValue = newValue
            }
        }
        
        cardDetailItems.append(CardDetailModel.init(withTitle: "Current Balance", withAmount: currentBalanceValue, withCurrency: currencyValue))
        cardDetailItems.append(CardDetailModel.init(withTitle: "Min. payment", withAmount: minPaymentValue, withCurrency: currencyValue))
        
        if let dueDateString = dueDateString {
            cardDetailItems.append(CardDetailModel.init(withTitle: "Due date", withDetail: dueDateString))
        }
    }
    
    func getCard(at index: Int) -> Card? {
        if carouselItems.count > 0 && index < carouselItems.count {
            return carouselItems[index].card
        } else {
            return nil
        }
    }
}
