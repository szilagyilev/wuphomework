//
//  CardDetailsCoordinator.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class CardDetailsCoordinator: Coordinator {
    
    // MARK: - Private Properties
    private let presenter: UINavigationController
    private var cardOverviewViewController: CardOverviewViewController?
    private var cardDetailsViewController: CardDetailsViewController?
    private var card: Card
    
    // MARK: - Initializers
    init(withPresenter newPresenter: UINavigationController, forCard newCard: Card) {
        self.presenter = newPresenter
        self.card = newCard
    }
    
    // MARK: - Public Functions
    func start() {
        let cardDetailsViewController = CardDetailsViewController()
        cardDetailsViewController.viewModel = CardDetailsViewModel.init(withCard: card)
        cardDetailsViewController.title = "Details"
        
        presenter.pushViewController(cardDetailsViewController, animated: true)
        self.cardDetailsViewController = cardDetailsViewController
    }
}
