//
//  AppCoordinator.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 03..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {

    // MARK: - Public Properties
    let window: UIWindow
    let rootViewController: UITabBarController
    
    // MARK: - Initializers
    init(window: UIWindow) {
        self.window = window
        rootViewController = UITabBarController()
        
        let cardOverViewNavigationController = UINavigationController()
        
        let cardOverviewCoordinator = CardOverviewCoordinator.init(withPresenter: cardOverViewNavigationController)
        
        let viewControllers : [UIViewController] = [cardOverViewNavigationController,
                                                    UnimplementedFunctionViewController(),
                                                    UnimplementedFunctionViewController(),
                                                    UnimplementedFunctionViewController()]
        
        rootViewController.viewControllers = viewControllers
        rootViewController.tabBar.tintColor = UIColor.ThemeColor.Base.Normal
        
        UITabBar.appearance().tintColor = UIColor.ThemeColor.Base.Dark
        UITabBar.appearance().unselectedItemTintColor = UIColor.ThemeColor.Base.Normal
        UITabBarItem.appearance().badgeColor = UIColor.ThemeColor.Accent.Dark
        
        if let cardOverview = rootViewController.tabBar.items?[0]{
            cardOverview.title = "Cards"
            cardOverview.image = UIImage(named: "iconCards")
        }
        
        if let transactionsOverview = rootViewController.tabBar.items?[1]{
            transactionsOverview.title = "Transactions"
            transactionsOverview.image = UIImage(named: "iconTransactions")
        }
        
        if let statementsOverview = rootViewController.tabBar.items?[2]{
            statementsOverview.title = "Statements"
            statementsOverview.image = UIImage(named: "iconStatements")
            statementsOverview.badgeValue = "3"
        }
        
        if let moreOverview = rootViewController.tabBar.items?[3]{
            moreOverview.title = "More"
            moreOverview.image = UIImage(named: "iconMore")
        }
        
        cardOverviewCoordinator.start()
    }
    
    // MARK: - Public Functions
    func start() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}
