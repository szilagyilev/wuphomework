//
//  CardOverviewCoordinator.swift
//  WUPHomework
//
//  Created by Levente Szilágyi on 2019. 07. 04..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class CardOverviewCoordinator: Coordinator {
    
    // MARK: - Private Properties
    private let presenter: UINavigationController
    private var cardOverviewViewController: CardOverviewViewController?
    private var cardDetailsCoordinator: CardDetailsCoordinator?
    
    // MARK: - Initializers
    init(withPresenter newPresenter: UINavigationController) {
        self.presenter = newPresenter
    }
    
    // MARK: - Public Functions
    func start() {
        let cardOverviewViewController = CardOverviewViewController()
        cardOverviewViewController.delegate = self
        cardOverviewViewController.viewModel = CardOverviewViewModel(withRemoteService: RemoteServiceImpl())
        
        cardOverviewViewController.title = "Card Overview"
        presenter.pushViewController(cardOverviewViewController, animated: true)
        
        self.cardOverviewViewController = cardOverviewViewController
    }
}

// MARK: - Extensions
extension CardOverviewCoordinator: CardOverviewViewControllerDelegate {
    func cardOverviewDidSelectCard(_ selectedCard: Card) {
        let cardDetailsCoordinator = CardDetailsCoordinator(withPresenter: presenter,
                                                            forCard: selectedCard)
        cardDetailsCoordinator.start()
        
        self.cardDetailsCoordinator = cardDetailsCoordinator
    }
}
